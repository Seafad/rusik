﻿public class TileModel
{
    public TileType type;

    public TileModel(TileType type)
    {
        this.type = type;
    }

    public enum TileType { Empty, Block, Chips, Spikes};
}

